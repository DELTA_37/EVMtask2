/****** w4.C ******/
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "CWin.h"
//===================================================
// Window win;
 Display *Global_disp=NULL;int Global_ScrNum=0;
 CWin *WinList[1000]; int NWinList=0;
 char *EventName(int n);
 char FontString[]="-cronyx-courier-medium-r-normal-*-*-140-*-*-m-*-koi8-r";
//===================================================
#define DEB 0
#define O(x) if(DEB)printf("%s\n",x);
//===================================================
CWin::CWin()
{ControlNum=0;win=0;disp=Global_disp;ScrNum=Global_ScrNum;
 WinList[NWinList++]=this;
 NCallBackFunctions=0;
 font=XLoadFont(disp,FontString); if(font==0)printf("Can't load font\n");
}

CWin::~CWin()
{int i;
printf("~CWin\n"); 
 if(font)XUnloadFont(disp,font);
 for(i=0;i<NWinList;i++)
  if(WinList[i]==this)
  {
   for(i++;i<NWinList;i++)
   WinList[i-1]=WinList[i];
   NWinList--;
  }
}

int CWin::Create(CWin *par,int x/*=0*/,int y/*=0*/, int width/*=200*/, int height/*=100*/,
            int EventsMasks/*=ExposureMask | KeyPressMask | ButtonPressMask*/,
            int black_pixel/*=12345678*/, int white_pixel/*=12345678*/)
{printf("1\n");
//  XSetWindowAttributes attr;
 win = XCreateSimpleWindow
 ( disp, par->win, x, y, width, height, 2,
// ( disp, par->win, x, y, 1000,1000, 2,
 (black_pixel==12345678?BlackPixel(  disp, ScrNum):black_pixel), 
 (white_pixel==12345678?WhitePixel(  disp, ScrNum):white_pixel) );
 border=2;
 left=x-border;
 top=y-border;
 right=x-border+width;
 bottom=y-border+height;
EventsMasks=EventsMasks;
 XSelectInput( disp, win, EventsMasks/*ExposureMask | KeyPressMask | ButtonPressMask*/);
 XMapWindow (disp,win);
   
EventsMasks=EventsMasks;
// XSelectInput( disp, win, EventsMasks/*ExposureMask | KeyPressMask | ButtonPressMask*/);
// XMapWindow (disp,win);
 XResizeWindow(disp,win,right-left,bottom-top);
 XSetWindowBorderWidth(disp,win,2);
// XChangeWindowAttributes(disp,win,0,&attr);
 font=XLoadFont(disp,FontString);
 
return 0;
}

int CWin::Create(int x/*=0*/,int y/*=0*/, int width/*=200*/, int height/*=100*/,
            int EventsMasks/*=ExposureMask | KeyPressMask | ButtonPressMask*/,
            int black_pixel/*=12345678*/, int white_pixel/*=12345678*/)
{printf("1\n");
//  XSetWindowAttributes attr;
 win = XCreateSimpleWindow
 ( disp, RootWindow( disp, ScrNum), x, y, width, height, 2,
// ( disp, RootWindow( disp, ScrNum), x, y, 1000,1000, 2,
 (black_pixel==12345678?BlackPixel(  disp, ScrNum):black_pixel), 
 (white_pixel==12345678?WhitePixel(  disp, ScrNum):white_pixel) );
 border=2;
 left=x-border;
 top=y-border;
 right=x-border+width;
 bottom=y-border+height;

EventsMasks=EventsMasks;
 XSelectInput( disp, win, EventsMasks/*ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask | ResizeRedirectMask*/);
 XMapWindow (disp,win);
 XResizeWindow(disp,win,right-left,bottom-top);
 XSetWindowBorderWidth(disp,win,2);
// XChangeWindowAttributes(disp,win,0,&attr);
 
return 0;
}

void CWin::OnSize(int cx,int cy)
{
 printf("W OnSize:%d %d\n",cx,cy);
 XResizeWindow(disp,win, cx,cy);
 Invalidate(1);
}
void CWin::OnMove(int l,int t,int r,int b)
{
 printf("W OnMove:%d %d %d %d\n",l,t,r,b);
 Invalidate(0);
}

void CWin::OnPaint(GC  gc)
{unsigned int w,h; int x,y; //GC gc;
//printf("OnPaint:1\n");
O("OnPaint 1")
 //XGetGeometry(disp,win,&root,&x,&y,&w,&h,&border,&depth);
 x=0;y=0;w=right-left-2*border; h=bottom-top-2*border;
 printf("OnPaint:win=%ld x=%d y=%d w=%d h=%d right=%d left=%d border=%d\n",win,x,y,w,h, right,left,border);
 XSetForeground(disp,gc,RGB(255*255,255*200,255*200));//WhitePixel(disp,ScrNum));
 XFillRectangle(disp,win,gc, x,y,w,h);
 //XFlush(disp);
O("OnPaint 5")
}
void CWin::OnLButtonClick(int x,int y)
{
printf("l:x=%d y=%d\n",x,y);
}
void CWin::OnRButtonClick(int x,int y)
{
printf("r:x=%d y=%d\n",x,y);
}
void CWin::OnCButtonClick(int x,int y)
{
printf("c:x=%d y=%d\n",x,y);
}
void CWin::OnLButtonClickUp(int x,int y)
{
printf("lu:x=%d y=%d\n",x,y);
}
void CWin::OnRButtonClickUp(int x,int y)
{
printf("ru:x=%d y=%d\n",x,y);
}
void CWin::OnCButtonClickUp(int x,int y)
{
printf("cu:x=%d y=%d\n",x,y);
}
void CWin::OnMouseMove(int x,int y)
{
printf("move:x=%d y=%d\n",x,y);
}
//void CWin::OnLButtonDblClick(int x,int y)
//{
//printf("l2:x=%d y=%d\n",x,y);
//}
void CWin::OnKeyPress(char c,int ExtendedSymbol)
{
printf("OnKeyPress: CotrolNum=%d; c='%c' ext=%d\n",ControlNum,c,ExtendedSymbol);
 if(ExtendedSymbol==XK_Delete)printf("Delete\n");
 if(ExtendedSymbol==XK_BackSpace)printf("BackSpace\n");
 if(ExtendedSymbol==XK_Left)printf("left\n");
 if(ExtendedSymbol==XK_Right)printf("right\n");
 if(ExtendedSymbol==XK_Up)printf("up\n");
 if(ExtendedSymbol==XK_Down)printf("down\n");
}


void CWin::Invalidate(int SetClear)
{
printf("Invalidate:width=%d height=%d\n",right-left,bottom-top);
 if(!SetClear)
 {
 static XEvent ev;
 int width=right-left,height=bottom-top;
 ev.xexpose.type=Expose;
 ev.xexpose.send_event=1;
 ev.xexpose.display=disp;
 ev.xexpose.window=win;
 ev.xexpose.x=ev.xexpose.y=0;ev.xexpose.width=width+1000;ev.xexpose.height=height+1000;ev.xexpose.count=0;
 XSendEvent(disp,win,1,ExposureMask,&ev);
 }
 else
 {
//  XClearArea(disp,win,0,0,0,0,1);//right-left,bottom-top,1);
  GC gc=XCreateGC(disp,win,0,0);//GCClipXOrigin|GCClipYOrigin,&gcv);
  XSetBackground(disp,gc,WhitePixel(disp,ScrNum));
  XSetForeground(disp,gc,WhitePixel(disp,ScrNum));
  XFreeGC(disp,gc);
  XClearWindow(disp,win);//right-left,bottom-top,1);
 //XFlush(disp);
//  XClearArea(disp,win,0,0,0,0,1);//right-left,bottom-top,1);
 int width=right-left,height=bottom-top;
 static XEvent ev;
 ev.xexpose.type=Expose;
 ev.xexpose.send_event=1;
 ev.xexpose.display=disp;
 ev.xexpose.window=win;
 ev.xexpose.x=ev.xexpose.y=0;ev.xexpose.width=width+1000;ev.xexpose.height=height+1000;ev.xexpose.count=0;
 XSendEvent(disp,win,1,ExposureMask,&ev);
 }
 XFlush(disp);
}

//===================================================
#include <unistd.h>
//char *data=new char[1000*1000];
int main()
{
 CWin *cw;
 Window wwin;
 Display *disp;
 int ScrNum;
// GC prGC;
 XEvent Evnt;
 int i=0;

 if ( (disp = XOpenDisplay (NULL)) == NULL)
  {
    printf("\n====1");
    exit(1);
  }

 ScrNum = DefaultScreen( disp );

 Global_disp=disp; Global_ScrNum=ScrNum;
 Xmain(disp,ScrNum);

 while( 1 )
  {
    XNextEvent( Global_disp, &Evnt);
    printf("Event %s\n",EventName(Evnt.type));
    if(Evnt.type==UnmapNotify||Evnt.type==DestroyNotify)
    {printf("Evnt.type==UnmapNotify||Evnt.type==DestroyNotify\n");}
    for(wwin=0,cw=NULL,i=0;i<NWinList;i++)
     if(WinList[i]->win==Evnt.xany.window)
     {cw=WinList[i]; wwin=Evnt.xany.window;break;}
    if(wwin==0||cw==NULL)
    {/*printf("Unknown window;i=%d\n",i);*/continue;}
    //---------------------------
    {
    switch (Evnt.type)
     {//Pixmap pm;
      case ButtonPress:
      O("ButtonPress")
        if(Evnt.xbutton.button==Button1)
	 cw->OnLButtonClick(Evnt.xbutton.x,Evnt.xbutton.y);
        if(Evnt.xbutton.button==Button3)
	 cw->OnRButtonClick(Evnt.xbutton.x,Evnt.xbutton.y);
        if(Evnt.xbutton.button==Button2)
	 cw->OnCButtonClick(Evnt.xbutton.x,Evnt.xbutton.y);
//        prGC = XCreateGC( disp, wwin, 0, NULL );
       break;
      case ButtonRelease:
        if(Evnt.xbutton.button==Button1)
	 cw->OnLButtonClickUp(Evnt.xbutton.x,Evnt.xbutton.y);
        if(Evnt.xbutton.button==Button3)
	 cw->OnRButtonClickUp(Evnt.xbutton.x,Evnt.xbutton.y);
        if(Evnt.xbutton.button==Button2)
	 cw->OnCButtonClickUp(Evnt.xbutton.x,Evnt.xbutton.y);
//        prGC = XCreateGC( disp, wwin, 0, NULL );
       break;
      case MotionNotify:
	 cw->OnMouseMove(Evnt.xmotion.x,Evnt.xmotion.y);
       break;
      case Expose:
      O("101")
        if ( Evnt.xexpose.count!=0 ) break;
	GC gc; XGCValues gcv; gcv.clip_x_origin=gcv.clip_y_origin=20;
//	 memset(data,-1,1000*1000);
        O("103")
        gc=XCreateGC(cw->disp,cw->win,0,0);//GCClipXOrigin|GCClipYOrigin,&gcv);
        if(cw->font)XSetFont(cw->disp,gc, cw->font);
	O("105")
	{
	O("110")
	 cw->OnPaint(gc);
	O("111")
	}
//	if(0)
        for(i=0;i<NWinList;i++)//if(0)
         if(WinList[i]->parent && WinList[i]->parent==cw)
	 {
	 if(0)
	 {
	  int rtn=0; XRectangle r;
	  r.x=0;//WinList[i]->left;
	  r.y=0;//WinList[i]->right;
	  r.height=WinList[i]->bottom-WinList[i]->top;
	  r.width=WinList[i]->right-WinList[i]->left;
	  rtn=XSetClipRectangles(cw->disp,gc,WinList[i]->left,WinList[i]->top,
	   &r,1,Unsorted);
        gcv.clip_x_origin=gcv.clip_y_origin=0;
        }
	O("120")
	//XGetGCValues(cw->disp,gc,GCClipXOrigin|GCClipYOrigin,&gcv);
	O("125")
//           printf("Origin:%d %d; rtn=%d clip=%d %d\n",	   WinList[i]->left,WinList[i]->top,rtn,	   gcv.clip_x_origin,gcv.clip_y_origin);	   
	   GC gc=XCreateGC(cw->disp,WinList[i]->win,0,0);//GCClipXOrigin|GCClipYOrigin,&gcv);
           if(WinList[i]->font)XSetFont(WinList[i]->disp,gc, WinList[i]->font);
	   WinList[i]->OnPaint(gc);
	   XFreeGC(cw->disp,gc);
	 }
	XFreeGC(cw->disp,gc);
	O("151")
       break;
      case ResizeRequest:
        {int w,h,border=cw->border;
         printf("ResizeRequest:win=%ld  w=%d h=%d\n",cw->win,Evnt.xresizerequest.width,Evnt.xresizerequest.height);
         w=Evnt.xresizerequest.width+2*border;
         h=Evnt.xresizerequest.height+2*border;
         cw->right=cw->left-border+w;
         cw->bottom=cw->top-border+h;
          cw->OnSize(Evnt.xresizerequest.width,Evnt.xresizerequest.height);
     //    cw->Invalidate(1);
        }
       break;
      case ConfigureNotify:
        if(cw->parent==NULL)
	{unsigned int w,h,border; int x,y;
         //XGetGeometry(disp,cw->win,&root,&x,&y,&w,&h,&border,&depth);
	 border=Evnt.xconfigure.border_width;
	 w=Evnt.xconfigure.width+2*border;
	 h=Evnt.xconfigure.height+2*border;
	 x=Evnt.xconfigure.x;
	 y=Evnt.xconfigure.y;
         printf("ConfigureNotify:w=%d r-l=%d border=%d\n",w,cw->right-cw->left,cw->border);
	 if((int)w==cw->right-cw->left && (int)h==cw->bottom-cw->top)
	 {
 	  cw->OnMove(x,y,x+w,y+h);
	 }
	 else
	 {
 	  cw->OnSize(Evnt.xconfigure.width,Evnt.xconfigure.height);
	 }
	 cw->border=border;
	 cw->left=x-border;
	 cw->top=y-border;
	 cw->right=x-border+w;
	 cw->bottom=y-border+h;
	}
        else
        {
         int border=0;border=Evnt.xconfigure.border_width;
         printf("Subwin ConfigureNotify: w=%d r-l=%d border=%d\n",Evnt.xconfigure.width+2*border,cw->right-cw->left,cw->border);
        }
        for(i=0;i<NWinList;i++)
         if(WinList[i]->parent && WinList[i]->parent==cw)
	 {
//         printf("Subwin: w=%d r-l=%d border=%d\n",Evnt.xconfigure.width+2*border,cw->right-cw->left,cw->border);
	 }
       break;
      case GravityNotify:
        cw->OnMove(Evnt.xgravity.x,Evnt.xgravity.y,0,0);
       break;
      case KeyPress:
      {
      unsigned int sym=XKeycodeToKeysym(cw->disp,Evnt.xkey.keycode,0);
      unsigned int sym_shift=XKeycodeToKeysym(cw->disp,Evnt.xkey.keycode,1);
//      if(XKeycodeToKeysym(cw->disp,Evnt.xkey.keycode,0)==XK_a)
//printf();
      if(Evnt.xkey.state&ShiftMask)//printf("Shift l\n");
      {printf("Shift l %c %c\n",sym,sym_shift);
       if(sym_shift>=XK_a && sym_shift<=XK_z)
        cw->OnKeyPress('a'+(sym_shift-XK_a));
       else if(sym_shift>=XK_A && sym_shift<=XK_Z)
        cw->OnKeyPress('A'+(sym_shift-XK_A));
       else if(sym_shift<128)
        cw->OnKeyPress(sym_shift);
	else
        cw->OnKeyPress(sym_shift,sym_shift);
      }
      else
      {
#define EK(key,ch) else if(sym==key)cw->OnKeyPress(ch);
       if(sym>=XK_a && sym<=XK_z)
        cw->OnKeyPress('a'+(sym-XK_a));
       else if(sym>=XK_A && sym<=XK_Z)
        cw->OnKeyPress('A'+(sym-XK_A));
       else if(sym>=XK_0 && sym<=XK_9)
        cw->OnKeyPress('0'+(sym-XK_0));
       else if(sym==XK_Return)
        cw->OnKeyPress('\n');
       else if(sym_shift<128)
        cw->OnKeyPress(sym);
	else
        cw->OnKeyPress(sym,sym);
      }
      }
       break;
      case DestroyNotify:
      //Evnt.xdestroywindow XDestroyWindowEvent
      printf("Destroy\n");
      break;
      case UnmapNotify:
      //Evnt.xdestroywindow XDestroyWindowEvent
      printf("Destroy\n");
      break;
     }
    }
    //---------------------------
//    le:;
  }
//fin:
 XCloseDisplay( disp );
return 0;
}

int CWin::RGB(int r,int g,int b)
{
 Colormap map=DefaultColormap(disp, ScrNum);
 XColor clr;
 clr.red=r;clr.green=g;clr.blue=b;
 clr.flags=DoRed|DoGreen|DoBlue;
 //int ierr=
 XAllocColor(disp,map,&clr);
return clr.pixel;
}
char *EventName(int n)
{static char s[100]; strcpy(s,"");
if ( n == KeyPress ) strcpy(s,"KeyPress");
else if ( n == KeyRelease ) strcpy(s,"KeyRelease");
else if ( n == ButtonPress ) strcpy(s,"ButtonPress");
else if ( n == ButtonRelease ) strcpy(s,"ButtonRelease");
else if ( n == MotionNotify ) strcpy(s,"MotionNotify");
else if ( n == EnterNotify ) strcpy(s,"EnterNotify");
else if ( n == LeaveNotify ) strcpy(s,"LeaveNotify");
else if ( n == FocusIn ) strcpy(s,"FocusIn");
else if ( n == FocusOut ) strcpy(s,"FocusOut");
else if ( n == KeymapNotify ) strcpy(s,"KeymapNotify");
else if ( n == Expose ) strcpy(s,"Expose");
else if ( n == GraphicsExpose ) strcpy(s,"GraphicsExpose");
else if ( n == NoExpose ) strcpy(s,"NoExpose");
else if ( n == VisibilityNotify ) strcpy(s,"VisibilityNotify");
else if ( n == CreateNotify ) strcpy(s,"CreateNotify");
else if ( n == DestroyNotify ) strcpy(s,"DestroyNotify");
else if ( n == UnmapNotify ) strcpy(s,"UnmapNotify");
else if ( n == MapNotify ) strcpy(s,"MapNotify");
else if ( n == MapRequest ) strcpy(s,"MapRequest");
else if ( n == ReparentNotify ) strcpy(s,"ReparentNotify");
else if ( n == ConfigureNotify ) strcpy(s,"ConfigureNotify");
else if ( n == ConfigureRequest ) strcpy(s,"ConfigureRequest");
else if ( n == GravityNotify ) strcpy(s,"GravityNotify");
else if ( n == ResizeRequest ) strcpy(s,"ResizeRequest");
else if ( n == CirculateNotify ) strcpy(s,"CirculateNotify");
else if ( n == CirculateRequest ) strcpy(s,"CirculateRequest");
else if ( n == PropertyNotify ) strcpy(s,"PropertyNotify");
else if ( n == SelectionClear ) strcpy(s,"SelectionClear");
else if ( n == SelectionRequest ) strcpy(s,"SelectionRequest");
else if ( n == SelectionNotify ) strcpy(s,"SelectionNotify");
else if ( n == ColormapNotify ) strcpy(s,"ColormapNotify");
else if ( n == ClientMessage ) strcpy(s,"ClientMessage");
else if ( n == MappingNotify ) strcpy(s,"MappingNotify");
return s;
}
//============================================
CSize CWin::GetTextExtent(GC gc, const char *str,int l)
{CSize sz(0,0); int direction, ascent,descent; XCharStruct overall;
 XQueryTextExtents(disp,font, str,(l>=0?l:strlen(str)), &direction, &ascent,&descent,&overall);
gc=gc;str=str;l=l;
return sz;
}
CSize CWin::GetTextExtent(const char *str,int l)
{CSize sz(0,0); int direction, ascent,descent; XCharStruct overall;
 GC gc; 
 gc=XCreateGC(disp,win,0,0);
 XSetFont(disp,gc,font);
 XQueryTextExtents(disp,font, str,(l>=0?l:strlen(str)), &direction, &ascent,&descent,&overall);
 XFreeGC(disp,gc);
 sz.cx=overall.width;sz.cy=ascent+descent;
gc=gc;str=str;l=l;
return sz;
}
//============================================
