/****** main.cpp ******/
#include "CWin.h"
#include <signal.h> 
void onsig(int s)
{printf("Killed:s=%d\n",s);exit(0);}
//===================================================
class CMyWin:public CWin
{public:
 CMyWin():CWin(){}
 SET_MESSAGE_MAP
 int OnButton1Click(){printf("CMyWin:button1\n");return 0;}
 int OnButton2Click(){printf("CMyWin:button2\n");return 0;}
};
//===================================================
BEGIN_MESSAGE_MAP(CMyWin)
 ON_COMMAND(101,OnButton1Click)
 ON_COMMAND(102,OnButton2Click)
END_MESSAGE_MAP
//===================================================
int Xmain(Display *disp,int ScrNum)
{disp=disp;ScrNum=ScrNum; int mask=0,i;
 signal(SIGSTOP,onsig);
 for(i=0;i<24;i++)mask=1|(mask<<1);printf("mask=%x\n",mask);
 CMyWin *w=new CMyWin();
  w->Create(100,100,300,300);//,mask);

  CButton *b1=new CButton();
  b1->Create(w,10,10,100,30,(char*)"Button1",101); 
  CButton *b2=new CButton();
  b2->Create(w,10,50,100,80,(char*)"Button2",102); 
return 0;
}

