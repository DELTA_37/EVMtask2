#include "CWin.hpp"
#include <iostream>
#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <math.h>


using namespace std;

bool lp1 = 1;
bool lp2 = 0;

bool del = 0;

vector<string> d;
string command;	
string version;


class CMainWindow : public CWin
{
public:
	CMainWindow(void) : CWin(){};
	~CMainWindow(void){};
	void OnLButtonClick(int x, int y);
	void OnPaint(GC gc)
	{
		unsigned int w, h; 
		int x, y; 
		x=0;
		y=0;
		w = right - left - 2 * border; 
		h = bottom - top - 2 * border;
		printf("OnPaint:win=%ld x=%d y=%d w=%d h=%d right=%d left=%d border=%d\n", win, x, y, w, h, right, left, border);
		XSetForeground(disp, gc, RGB(255 * 255, 255 * 200, 255 * 200));//WhitePixel(disp,ScrNum));
		XFillRectangle(disp, win, gc, x, y, w, h);

		XGCValues value;	
		unsigned long valuemask;
		GC gcl;

		value.foreground = 0;
		value.line_width = 3;
		value.fill_style = FillSolid;
		value.line_style = LineSolid;

		valuemask = GCLineStyle | GCLineWidth | GCFillStyle | GCForeground;

		gcl = XCreateGC(disp, win, valuemask, &value);

		int i;
		XSetForeground(disp, gc, BlackPixel(disp, ScrNum));
		XSetBackground(disp, gc, WhitePixel(disp, ScrNum));
		for (i = 0; i < d.size(); i++)
		{
			if ((d[i].size() > 4)&&(d[i].substr(0, 4) == "line"))
			{
				istringstream cur(d[i].substr(5));
				int x1, x2, y1, y2;
				cur >> x1 >> y1 >> x2 >> y2;
				XDrawLine(disp, win, gcl, x1, y1, x2, y2);
				XFlush(disp);
			}
		}
	}
	void OnSize(int cx,int cy)
	{
		Invalidate(0);
		cx=cx;
		cy=cy;
	}
};

class CMainButton : public CButton
{
public:
	CMainButton(void) : CButton(){};
	~CMainButton(void)  {};
	void OnLButtonClick(int x, int y)
	{
		if (this->id == 0)
		{
			lp1 = 1;
			lp2 = 0;
			del = 0;
		}
		else if (this->id == 1)
		{
			lp1 = 0;
			lp2 = 0;
			del = 1;
		}
		else if (this->id == 2)
		{
			// extract new.pgf
			ifstream in;
			string s;
			int n;
			in.open("new.gpf");
			
			d.clear();
			in >> version;
			in >> n;
			getline(in, s);
			for (int i = 0; i <= n; i++)
			{
				getline(in, s);
				d.push_back(s);
				cout << s << endl;
				s = "";
				fflush(stdout);
			}
			in.close();
		}
		else if (this->id == 3)
		{
			// save new.pgf
			ofstream out;
			out.open("new.gpf");
			out << version << endl;
			out << d.size() << endl;
			for (int i = 0; i < d.size(); i++)
			{
				out << d[i] << endl;
			}
			out.close();
		}
	}
};


bool Belong(string line, int a, int b)
{
	istringstream out(line.substr(5));
	int x1, y1, x2, y2;
	out >> x1 >> y1 >> x2 >> y2;
	cout << x1 << endl 
	     << y1 << endl
	     << x2 << endl
	     << y2 << endl;
	cout << endl;
	cout << a << endl
	     << b << endl;
	for (double t = 0; t <= 1; t += 1.0/(abs(x2 - x1) + abs(y2 - y1)))
	{
		double x = x1 + t*(x2 - x1);
		double y = y1 + t*(y2 - y1);
		if ((a - x)*(a - x) + (b - y)*(b - y) < 3)
		{
			return 1;
		}
	}
	return 0;
}

void CMainWindow::OnLButtonClick(int x, int y)
{
	printf("My Button Press\n");
	if (lp1 == 1)
	{
		lp1 = 0;
		lp2 = 1;
		del = 0;
		ostringstream cur;
		cur << "line ";
		cur << x << " " << y << " ";
		d.push_back(cur.str());

		printf("\ngot\n\n");

	}
	else if (lp2 == 1)
	{
		lp1 = 1;
		lp2 = 0;
		del = 0;
		ostringstream cur;
		cur << d[d.size() - 1];
		cur << x << " " << y;
		d[d.size() - 1] = cur.str();

		printf("\ndraw\n\n");
		GC gc; XGCValues gcv; 
		gcv.clip_x_origin = gcv.clip_y_origin = 20;
		gc = XCreateGC(this->disp, this->win, 0, 0);
		if(this->font)
		{
			XSetFont(this->disp, gc, this->font);
		}
		this->OnPaint(gc);
	}
	else if (del == 1)
	{
		lp1 = 0;
		lp2 = 0;
		del = 1;
		for (int i = 0; i < d.size(); i++)
		{
			if (Belong(d[i], x, y))
			{
				d.erase(d.begin() + i);
				break;
			}
		}

		printf("\ndel\n\n");
		GC gc; XGCValues gcv; 
		gcv.clip_x_origin = gcv.clip_y_origin = 20;
		gc = XCreateGC(this->disp, this->win, 0, 0);
		if(this->font)
		{
			XSetFont(this->disp, gc, this->font);
		}
		this->OnPaint(gc);

	}
}

int Xmain(_XDisplay* disp, int scr_num)
{
	version = "1.1";
	CMainWindow* wind1 = new CMainWindow();
	wind1->Create(0, 0, 200, 500);
	CMainButton* bline = new CMainButton();
	bline->Create(wind1, 0, 0, 50, 50, "line", 0);
	CMainButton* bdelete = new CMainButton();
	bdelete->Create(wind1, 51, 0, 50, 50, "delete", 1);
	CMainButton* bextract = new CMainButton();
	bextract->Create(wind1, 102, 0, 50, 50, "extract", 2);
	CMainButton* bsave = new CMainButton();
	bsave->Create(wind1, 153, 0, 50, 50, "save", 3);
	return 0;
}
