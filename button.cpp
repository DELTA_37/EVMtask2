/****** button.cpp ******/
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "CWin.h"
//=====================================================================
CButton::CButton() : CWin()
{
 ControlNum=1;
 text[0]='\0';
 left=top=right=bottom=0;
 parent =NULL;
 IsClicked=0;
 TextColor=RGB(0,0,0);
}

CButton::~CButton()
{
}

void CButton::OnLButtonClick(int x,int y)
{
 printf("LButton %s clicked (%d %d)\n",text,x,y);
 IsClicked=1;
 Invalidate();
 parent->SetMessageMap(id);
}

void CButton::OnLButtonClickUp(int x,int y)
{
 printf("LButtonU %s clicked (%d %d)\n",text,x,y);
 IsClicked=0;
 Invalidate();
}

void CButton::OnRButtonClick(int x,int y)
{
 printf("RButton %s clicked (%d %d)\n",text,x,y);
}

void CButton::OnCButtonClick(int x,int y)
{
 printf("CButton %s clicked (%d %d)\n",text,x,y);
}

int CButton::Create(CWin *parent,int l,int t,int r,int b,const char *s,int id)
{
 strcpy(text,s);
 this->parent=parent;
 this->left=l;
 this->right=r;
 this->top=t;
 this->bottom=b;
 this->id=id;
 
 win = XCreateSimpleWindow
	  ( disp, parent->win, l, t, r-l, b-t, 0,
	     BlackPixel(  disp, ScrNum),
	     WhitePixel(  disp, ScrNum) );
 XSelectInput(disp,win,ExposureMask | KeyPressMask | ButtonPressMask |
	        ButtonReleaseMask |StructureNotifyMask|PointerMotionMask);
//ExposureMask|KeyPressMask|ButtonPressMask|ButtonReleaseMask);
 XMapWindow(disp,win);
return 0;
}

int CButton::Create(CWin *parent,int l,int t,const char *s,int id)
{int r,b;
 CSize sz=parent->GetTextExtent(s);printf("extent:%d %d\n",sz.cx,sz.cy);
 r=l+5+sz.cx;
 b=t+5+sz.cy;
 strcpy(text,s);
 this->parent=parent;
 this->left=l;
 this->right=r;
 this->top=t;
 this->bottom=b;
 this->id=id;
 win = XCreateSimpleWindow
	  ( disp, parent->win, l, t, r-l, b-t, 0,
	     BlackPixel(  disp, ScrNum),
	     WhitePixel(  disp, ScrNum) );
 XSelectInput(disp,win,ExposureMask | KeyPressMask | ButtonPressMask |
	        ButtonReleaseMask |StructureNotifyMask|PointerMotionMask);
//ExposureMask|KeyPressMask|ButtonPressMask|ButtonReleaseMask);
 XMapWindow(disp,win);
return 0;
}

void CButton::OnPaint(GC gc)
{
 XSetForeground(disp,gc,TextColor);printf("textcolor=%d\n",TextColor);
//  SetForeground(gc,RGB(65025,0,0));
   XDrawRectangle(disp,win,gc,1,1,right-left-2,bottom-top-2);
   XDrawString(disp,win,gc,+3+(IsClicked?1:0),bottom-top-3+(IsClicked?1:0),text,strlen(text));
}
