#ifndef __CWin_H__
#define __CWin_H__
/****** CWin.h ******/
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/keysym.h>
#include <X11/keysymdef.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
//===================================================
extern Window win;
extern Display *Global_disp/*=NULL*/;extern int Global_ScrNum/*=0*/;
//===================================================
class CCallbackFunction
{
public:
	int id;
	int (*fun)(void);
};

class CSize
{
public:
	int cx,cy;
	CSize(){cx=0;cy=0;}
	CSize(int x,int y){cx=x;cy=y;}
};
//===================================================
class CWin 
{
public:
	//-----------------
	Display *disp; int ScrNum;
	int left,top,right,bottom,border;
	int ControlNum;
	Window win;
	CWin *parent;
	Font font;
	//-----------------
	CCallbackFunction CallBackFunctions[1000];
	int NCallBackFunctions;
	//-----------------
	CWin();
	virtual ~CWin();
	int Create(int x=0,int y=0, int width=200, int height=100,
	    	int EventsMasks=ExposureMask | KeyPressMask | ButtonPressMask |
		ButtonReleaseMask|StructureNotifyMask|PointerMotionMask,//|ResizeRedirectMask,
	    	int black_pixel=12345678, int white_pixel=12345678);
	int Create(CWin *parent, int x=0,int y=0, int width=200, int height=100,
		int EventsMasks=ExposureMask | KeyPressMask | ButtonPressMask |
		ButtonReleaseMask |StructureNotifyMask|PointerMotionMask,//|ResizeRedirectMask,
		int black_pixel=12345678, int white_pixel=12345678);
	virtual void SetMessageMap(int ID){ID=0;}
	int RGB(int red,int green,int blue);//each component: 0 - 65025
	//-----------------
	void SetBackground(GC gc, int color){XSetBackground(disp,gc,color);}
	void SetForeground(GC gc, int color){XSetForeground(disp,gc,color);}
	//-----------------
	virtual void OnPaint(GC gc);
	virtual void OnSize(int cx,int cy);
	virtual void OnMove(int l,int t,int r,int b);
	virtual void OnLButtonClick(int x,int y);
	virtual void OnRButtonClick(int x,int y);
	virtual void OnCButtonClick(int x,int y);
	virtual void OnLButtonClickUp(int x,int y);
	virtual void OnRButtonClickUp(int x,int y);
	virtual void OnCButtonClickUp(int x,int y);
	virtual void OnMouseMove(int x,int y);
	virtual void OnKeyPress(char c,int ExtendedSymbol=0);
	//ExtendedSymbol: XK_Return XK_Left XK_Right XK_Down XK_Up XK_BackSpace XK_Delete
	//-----------------
	void Invalidate(int SetClear=1);
	CSize GetTextExtent(const char *str,int l=-1);
	CSize GetTextExtent(GC gc, const char *str,int l=-1);
	//-----------------
	// virtual void OnLButtonDblClick(int x,int y);
};
//===================================================
class CButton : public CWin
{ 
public:
	CButton();
	int Create(CWin *parent, int left,int top,int right,int bottom, const char *Text="Button",int id=0);
	int Create(CWin *parent, int left,int top,const char *Text="Button",int id=0);
	virtual ~CButton();
	virtual void OnLButtonClick(int x,int y);
	virtual void OnLButtonClickUp(int x,int y);
	virtual void OnRButtonClick(int x,int y);
	virtual void OnCButtonClick(int x,int y);
	virtual void OnPaint(GC gc);
	virtual void SetTextColor(int clr){TextColor=clr;}

	char text[128];
	int IsClicked;
	int id;
	int TextColor;
};
//===================================================
int Xmain(Display *disp, int ScrNum);
//===================================================
#define SET_MESSAGE_MAP virtual void SetMessageMap(int FID);
//===================================================
#define BEGIN_MESSAGE_MAP(CMyWin) void CMyWin::SetMessageMap(int FID){
#define  ON_COMMAND(ID,OnFun)  if(ID==FID) OnFun();
#define END_MESSAGE_MAP }
//===================================================
#endif

